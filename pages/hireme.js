import Link from 'next/link';
import Layout from '../components/Layout/Layout';


const Hireme = props => {
  return (
    <Layout title="Hire Me">
      <h1>Hire me</h1>
      <p>
        You can hire me at{' '}
        <a href="mailto:alexander.bryksin@yandex.ru">
          alexander.bryksin@yandex.ru
        </a>{' '}
      </p>
    </Layout>
  );
};

export default Hireme;
