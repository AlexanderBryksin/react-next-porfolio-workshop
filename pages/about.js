import Link from 'next/link';
import Layout from '../components/Layout/Layout';
import { Component } from 'react';
import fetch from 'isomorphic-unfetch';
import { withRouter } from 'next/router';
import Error from './_error';

class About extends Component {
  static async getInitialProps() {
    const res = await fetch('https://api.github.com/users/aleksk1ng');
    const statusCode = res.status > 200 ? res.status : false;
    const data = await res.json();
    return { user: data, statusCode };
  }
  //
  // componentDidMount() {
  //   fetch('https://api.github.com/users/aleksk1ng')
  //     .then(res => res.json())
  //     .then(data => {
  //       this.setState({ user: data });
  //       console.log(data);
  //     })
  //     .catch(err => {
  //       console.log(err);
  //     })
  // }

  render() {
    const { user, statusCode } = this.props;

    if (statusCode) {
      return <Error statusCode={statusCode} />;
    }

    return (
      <Layout title="About">
        <p>{user.name}</p>
        <img src={user.avatar_url} alt="Me" height="200px" />
      </Layout>
    );
  }
}

export default About;

//   About.getInitialProps = () => {
//
// }
