import Layout from '../components/Layout/Layout';
import Link from 'next/link';

const PostLink = ({ title, slug }) => (
  <li>
    <Link as={`/${slug}`} href={`/post?title=${title}`}>
      <a>{title}</a>
    </Link>
  </li>
);

const Blog = props => {
  return (
    <Layout title="My Blog">
      <ul>
        <PostLink slug="react-post" title="React" />
        <PostLink slug="redux-post" title="Redux" />
        <PostLink slug="next.js-post" title="Next.js" />
      </ul>
    </Layout>
  );
};

export default Blog;
