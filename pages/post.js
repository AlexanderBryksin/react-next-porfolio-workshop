import Layout from '../components/Layout/Layout';
import { withRouter } from 'next/router';

const Post = ({ router }) => {
  return (
    <Layout title={router.query.title}>
      <p
        style={{
          width: '80wh'
        }}
      >
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. At aut nemo
        officiis quibusdam vel veniam? Cumque facilis harum hic libero officia
        quia sed tempore ut!
      </p>
    </Layout>
  );
};

export default withRouter(Post);
