import Layout from '../components/Layout/Layout';

const Index = () => (
  <Layout title="Home">
    <p>Welcome to the my home Next.js page ! </p>
  </Layout>
);

export default Index;
